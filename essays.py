import util
from nltk.tokenize import sent_tokenize

class Graded_Essays:
   
   def __init__(self, essay_id = 0, essay_set = 0, essay = None, 
                rater1_domain1 = 0, rater2_domain1 = 0, rater3_domain1 = 0, domain1_score = 0, 
                rater1_domain2 = 0, rater2_domain2 = 0, domain2_score = 0, 
                rater1_trait1 = None, rater1_trait2 = None, rater1_trait3 = None, rater1_trait4 = None, rater1_trait5 = None, rater1_trait6 = None, 
                rater2_trait1 = None, rater2_trait2 = None, rater2_trait3 = None, rater2_trait4 = None, rater2_trait5 = None, rater2_trait6 = None, 
                rater3_trait1 = None, rater3_trait2 = None, rater3_trait3 = None, rater3_trait4 = None, rater3_trait5 = None, rater3_trait6 = None):
      self.essay_id = util.str_to_int(essay_id)
      self.essay_set = util.str_to_int(essay_set)
      self.essay = essay
      self.domain1grade1 = util.str_to_int(rater1_domain1)
      self.domain1grade2 = util.str_to_int(rater2_domain1)
      self.domain1grade3 = util.str_to_int(rater3_domain1)
      self.overall_domain1 = util.str_to_int(domain1_score)
      self.domain2grade1 = util.str_to_int(rater1_domain2)
      self.domain2grade2 = util.str_to_int(rater2_domain2)
      self.overall_domain2 = util.str_to_int(domain2_score)
      
      
      
      
      self.rater1_traits = [rater1_trait1, rater1_trait2, rater1_trait3, rater1_trait4, rater1_trait5, rater1_trait6]
      self.rater2_traits = [rater2_trait1, rater2_trait2, rater2_trait3, rater2_trait4, rater2_trait5, rater2_trait6]
      self.rater3_traits = [rater3_trait1, rater3_trait2, rater3_trait3, rater3_trait4, rater3_trait5, rater3_trait6]

      self.rater1_traits = [util.str_to_int(x) for x in self.rater1_traits]
      self.rater2_traits = [util.str_to_int(x) for x in self.rater2_traits]
      self.rater3_traits = [util.str_to_int(x) for x in self.rater3_traits]

      self.normalized_scores = {1 : (2, 12), 2 : (1, 6), 3 : (0, 3), 4 : (0, 3), 
                                5 : (0, 4), 6 : (0, 4), 7 : (0, 30), 8 : (0, 60)}
      self.normalize_scores()

   def __str__(self):
      text = "essay id: " + self.essay_id + "\n"
      text += "essay set: " + self.essay_set + "\n"
      text += "essay : " + self.essay + "\n"
      text += "domain 1 grades: " + "\t".join([self.domain1grade1, self.domain1grade2, self.domain1grade3]) + "\n"
      text += "domain 1 overall grade: " + self.overall_domain1 + "\n"
      text += "domain 2 grades: " + "\t".join([self.domain2grade1, self.domain2grade2]) + "\n"
      text += "domain 2 overall grade: " + self.overall_domain2 + "\n"
      
      text += "rater 1 traits: " + "\t".join(self.rater1_traits) + "\n"
      text += "rater 2 traits: " + "\t".join(self.rater2_traits) + "\n"
      text += "rater 3 traits: " + "\t".join(self.rater3_traits) + "\n"

      return text

   def normalize_scores(self):
      """
      Only normalizes the overall grades
      """
      min_val, max_val = self.normalized_scores[self.essay_set]
      self.overall_domain1 = (self.overall_domain1 - min_val) / (max_val - min_val)

      if self.essay_set == 2:
         self.overall_domain2 = self.overall_domain2 - 1 / 3
      
   def get_sentences(self):
      return sent_tokenize(self.essay)

   def get_paragraphs(self):
      return [self.essay]

class Ungraded_Essays:
   def __init__(self, nid = None, title = None, paragraphs = [], annotating_teacher_id = None, mistakes = []):
      self.nid = nid
      self.title = title
      self.paragraphs = paragraphs
      self.annotating_teacher_id = annotating_teacher_id
      self.mistakes = mistakes

   def __str__(self):
      text = "nid: " + self.nid + "\n"
      text += "Title: " + self.title + "\n"
      text += "Paragraphs: " + "\n".join(self.paragraphs) + "\n"
      text += "Annotating teacher id: " + self.annotating_teacher_id + "\n"
      return text

   def correct_mistakes(self):
      for mistake in self.mistakes:
         if mistake.correction != None:
            paragraph = self.paragraphs[mistake.start_par]
            new_par = paragraph[ : mistake.start_char] + " " + mistake.correction + " " + paragraph[mistake.end_char :]
            self.paragraphs[mistake.start_par] = new_par.replace("  ", " ")

   def get_full_text(self):
      return '\n'.join(self.paragraphs)

   def get_sentences(self):
      sents = []
      for p in self.paragraphs:
         sents.extend(sent_tokenize(p))
      return sents

   def get_paragraphs(self):
      return self.paragraphs

class Mistake:
   def __init__(self, start_par, start_char, end_par, end_char, error_type, correction, comment):
      self.start_par = start_par
      self.start_char = start_char
      self.end_par = end_par
      self.end_char = end_char
      self.error_type = error_type
      self.correction = correction
      self.comment = comment

   def __str__(self):
      text = "Starting Paragraph: " + self.start_par + "\n"
      text += "Starting Character: " + self.start_char + "\n"
      text += "Ending Paragraph: " + self.end_par + "\n"
      text += 'Ending Character: ' + self.end_char + "\n"
      text += "Error Type: " + self.error_type + "\n"
      text += 'Correction: ' + self.correction + "\n"
      text += 'Comment: ' + self.comment + "\n"

      return text
