#!/usr/local/bin/python3

import language_check

tool = language_check.LanguageTool('en-US')

def main():

   """
   Thanks to:
   http://blog.hubspot.com/marketing/common-grammar-mistakes-list#sm.000vnxuyz18qvdntr001pd32eozz9

   For some test sentences.
   """

   general_tests = ("General Tests", [
                    u'A sentence with a error in the Hitchhiker’s Guide tot he Galaxy',
                    u'Dis sentense haz lot of speling mitakes in it.'])

   your_tests = ("Testing Your vs You're", [
                 u"Your a cutie!",
                 u"How is you're dog?"])

   its_test = ("Testing it vs it's", [
               u"Its a dog.",
               u"It's dog is very nice."])
   
   possession_tests = ("Testing Possessive Nouns", [
                       u"The lizards tail.",
                       u"The dogs's bones.",
                       u"The dress's blue color."])

   affect_tests = ("Testing Affect vs Effect", [
                   u"That movie had a great affect on me.",
                   u"That movie effected me greatly.",
                   u"That movie effects me greatly."])

   there_tests = ("Testing There vs Their vs They're", [
                  u"Their is a boat.",
                  u"They're is a boat.",
                  u"They are is a boat.",
                  u"That is they're boat.",
                  u"There a very nice boat."])

   contractions_tests = ("Testing Contractions - Can't, Shouldn't, etc.", [
                         u"You can't not do that.",
                         u"You really shouldn't not not do that."])

   into_tests = ("Testing Into vs In To", [
                 u"I walked in to the office."
                 u"I was called into a meeting."])

   alot_tests = ("Testing A Lot and Allot", [
                 u"I do that alot.",
                 u"I've aloted $20 for gas."])

   lose_tests = ("Testing Lose vs Loose", [
                 u"My shirt is lose fitting.",
                 u"I hate when we loose."])

   then_tests = ("Testing Then vs Than", [
                 u"My dinner was better then yours.",
                 u"I went to school than I went home."])

   assure_tests = ("Testing Assure vs Ensure", [
                   u"I ensure you that he's good at his job.",
                   u"Assure you're free when I visit next weekend."])

   compliment_tests = ("Testing Compliment vs Complement", [
                       u"The win compliments the meal wonderfully.",
                       u"I love getting complements!"])

   all_tests = [general_tests, your_tests, its_test, possession_tests, affect_tests, there_tests,
                contractions_tests, into_tests, alot_tests, lose_tests, then_tests,
                assure_tests, compliment_tests]
   
   for tests in all_tests:
      run_tests(tests)

   test_sents = [u'A sentence with a error in the Hitchhiker’s Guide tot he Galaxy',
                 u'Dis sentense haz lot of speling mitakes in it.',
                 u'Your a cutie.',
                 u'Its a dog.',
                 u'Our car model is faster, better, stronger.',
                 u'After declining for months, Jean tried a new tactic to increase ROI.',
                 u"The lizards tail.",
                 u"The dogs' bones.",
                 u"The dress's blue color.",
                 u"That movie had a great affect on me.",
                 u"That movie effected me greatly.",
                 u"That movie effects me greatly.",
                 u"When you get done with that lab report, can you send it to Bill and I?",
                 u"Their is a boat.",
                 u"They're is a boat.",
                 u"They are is a boat.",
                 u"That is they're boat.",
                 u"There nice.",
                 u"You can't not do that.",
                 u"You really shouldn't not not do that.",
                 u"Her computer is the one who overheats all the time.",
                 u"I walked in to the office.",
                 u"I was called into a meeting.",
                 u"I do that alot.",
                 u"I've aloted $20 for gas.",
                 u"My shirt is lose fitting.",
                 u"I hate when we loose.",
                 u"My dinner was better then yours.",
                 u"I went to school than I went home.",
                 u"I ensure you that he's good at his job.",
                 u"Assure you're free when I visit next weekend.",
                 u"The wine compliments the meal wonderfully.",
                 u"I love getting complements!",
                 ]
   
   #for sent in test_sents:
   #   test_sentence(sent)
   
def run_tests(tests):
   name, sents = tests

   print("="*80)
   print("### " + name + " ###")

   for sent in sents:
      test_sentence(sent)
   
   print("")

def test_sentence(sent):

   print(("  -"*26))
   print("Checking Sentence:\n'{}'".format(sent))
   matches = tool.check(sent)
   print("Found {} problems".format(len(matches)))

   for match in matches:
      print(match)
      print("")
   
   fix_sent = language_check.correct(sent, matches)
   print("Corrected Sentence:\n'{}'".format(fix_sent))
   print("")


if __name__ == "__main__":
   main()

