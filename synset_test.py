from nltk.corpus import wordnet as wn

dog = wn.synset('dog.n.01')

print("Dog Hypernyms")
print(dog.hypernyms())

print("")
print("Dog Hyponyms")
print(dog.hyponyms())  # doctest: +ELLIPSIS

print("")
print("Dog Member Holonyms")
print(dog.member_holonyms())

print("")
print("Dog Root Hypernyms")
print(dog.root_hypernyms())

print("")
print("Dog lowest common hypernyms Cat")
print(wn.synset('dog.n.01').lowest_common_hypernyms(wn.synset('cat.n.01')))

print("")
print("Dog lowest common hypernyms Puppy")
print(wn.synset('dog.n.01').lowest_common_hypernyms(wn.synset('puppy.n.01')))


print("")
print(wn.synsets('walk'))

# Topic lists are from lab3
wordlist1 = ['senate','cruz','hampshire','people','great','bernie','meeting',
        'contribution','emily list','time','donald','states','donate',
        'candidate','town hall','just','make','emily','hillary','clinton',
        'kasich','fight','campaign','today','going']
wordlist2 = ['state','states','support','great','president','today','know',
        'hampshire','campaign','america','people','fight','country','make',
        'political','contribution','time','trump','friend','meeting','cruz',
        'just','john','bernie','donald']

# assumes that this line exists somewhere:
#from nltk.corpus import wordnet as wn
def topic_sim_path(t1,t2):
    similarity = 0.0
    count = 0
    for w1 in t1:
        w1_synsets = wn.synsets(w1)
        for w2 in t2:
            w2_synsets = wn.synsets(w2)
            for syn1 in w1_synsets:
                for syn2 in w2_synsets:
                    sim = syn1.path_similarity(syn2)
                    count += 1
                    if sim is None:
                        # Case where there is not common root
                        similarity += 0
                    else:
                        similarity += sim
    if count == 0:
        return 0.0
    return similarity/count

print("MOUSE TEST TIME")
print(wordlist1)
print(wordlist2)
print(topic_sim_path(wordlist1,wordlist2))
print(topic_sim(['state'],['state']))
print(topic_sim(['state'],['state'], "wup"))
print(topic_sim(['state'],['state'], "lch"))
print(topic_sim([],[]))
print(topic_sim([],[], "wup"))
print(topic_sim([],[], "lch"))

