import csv
from essays import Graded_Essays

def parse_graded_essays(filepath):
   graded_dataset = []
   with open(filepath, "r", errors ='ignore') as essays:
      reader = csv.DictReader(essays, delimiter = "\t")
      for row in reader:
         graded_dataset.append(Graded_Essays(**row))

   return graded_dataset

if __name__ == '__main__':
   parse_graded_essays("corpora/hewlett-automated-essay-scoring/data/training_set_rel3.tsv")
