
from multiprocessing import Pool, Process, Queue, JoinableQueue
import queue 
import traceback
from functools import partial
from timer import Timer
import sys

# Progress Monitored Task -----------------------------------------------------
def monitor_progress(processes, task_func, tasks, quiet_kill = False):
   
   if processes <= 0:
      raise ValueError("Given number of processes must be greater than zero!")

   num_tasks = len(tasks)

   if num_tasks < processes:
      processes = num_tasks

   func_partial = partial(_safe_dispatch_function_wrapper, task_func, quiet_kill)
   task_queue = JoinableQueue()
   results_queue = JoinableQueue()  
   
   args = (task_queue, results_queue, func_partial)
   results = []

   for task in tasks:
      task_queue.put(task)

   procs = [Process(target = _worker, args = args) for _ in range(processes)]
   for p in procs:
      p.start()
   
   timer = Timer(start_now = True)
   msg_fmt = "Processing Task {:4d} / {:4d}  {}     \r"
   for task_num in range(num_tasks):
      sys.stdout.write(msg_fmt.format(task_num, num_tasks, 
                                      timer.make_str(est = (num_tasks - task_num))))
      sys.stdout.flush()
      results.append(results_queue.get()) 
      results_queue.task_done()
      timer.lap()
   
   print("Finished Processing {} Tasks  {} {}".format(num_tasks, timer.make_str(), " "*40))

   results_queue.join() 
   task_queue.join()
   for p in procs:
      p.join()
   
   results_queue.close()
   task_queue.close()

   return results

def _worker(input_queue, output_queue, func):
   
   while not input_queue.empty():
      try:
         task = input_queue.get(block = True, timeout = 2)
         result = func(task)
         output_queue.put(result)
         input_queue.task_done()
      except queue.Empty:
         break
   
   output_queue.close()


# Multiprocessing Pool Utilities ----------------------------------------------
def safe_dispatch(proc_pool, task_func, tasks, quiet_kill = False):
   """
   Safely dispatches the given tasks with the given function to the given
   process pool while still allowing interrupts to properly clean up the 
   active processes. This function will block until the tasks are complete.
   
   Arguments:
      |proc_pool| the process pool to use
      |function| the function to call for each task
      |tasks| the list of tasks to complete
   
   Optional:
      |quiet_kill| defaults to True, when True this will suppress the stderr in
         the worker processes. This is particularly useful in the event of a
         keyboard interrupt which will typically cause the screen to be flooded
         with tracebacks from every single child process. This is rarely useful
         however if this behavior is desired setting |quiet_kill| to False will
         leave stderr as is.

   Returns a list of the results from calling the given function for each of
      the given tasks.
   """
    
   func_partial = partial(_safe_dispatch_function_wrapper, task_func, quiet_kill)
   try:
      return proc_pool.map_async(func_partial, tasks).get(0xFFFF)
   except KeyboardInterrupt:
      print("\nGot Keyboard Interrupt - Terminating Process Pool")
      print("You may need to press ^C again if this hangs.")
      proc_pool.terminate() 
      proc_pool.join()
      raise

def _safe_dispatch_function_wrapper(func, quiet_kill, task):
   """
   Helper function for safe_dispatch() 
   Not intended to be called directly.
   Not defined inside safe_dispatch() as it must be in the global scope to work
   with multiprocessing.
   """
   if quiet_kill: sys.stderr = os.devnull
   try:
      return func(task)
   except KeyboardInterrupt:
      pass
   except:
      traceback.print_exc()

