
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from scipy.stats import gmean, hmean
import numpy as np
from lda import LDA 

from essays import Ungraded_Essays
import random 

import synset


class TopicAnalysis(object):
   def __init__(self, essay):
      self.essay = essay
      self.n_topics = len(essay.get_sentences()) * 1

      self.lda = LDA(self.n_topics, TfidfVectorizer, max_iter = 1000, 
                     evaluate_every = 10, verbose = 0)
      self.lda.fit_essay(essay)

      self.useless_topics, self.useful_topics, self.topic_usage = self.lda.topic_usage()

   def num_useful_topics(self):
      return len(self.useful_topics)
   
   def get_topic_usage(self):
      return self.topic_usage


   def _item_continuity(self, item_list, n_words = 10, generalized = False):
      if generalized:
         tbi = self.lda.predict_generalized(item_list)
      else:
         tbi = self.lda.predict_best_only(item_list)

      item_conts = []
      n_words = 10
      for item1_word_dist, item2_word_dist in zip(tbi[:-1], tbi[1:]):
         
         item1_words = self.lda.get_topic_words(item1_word_dist, n_words)
         item2_words = self.lda.get_topic_words(item2_word_dist, n_words)
         
         sim = synset.topic_sim(item1_words, item2_words)
         item_conts.append(sim)
      
      return item_conts


   def topics_by_sentence(self): 
      return self.lda.predict_best_only(self.essay.get_sentences())

   def sentence_continuity(self, n_words = 10, generalized = False):
      return self._item_continuity(self.essay.get_sentences(), n_words, generalized)


   def topics_by_paragraph(self): 
      return self.lda.predict_best_only(self.essay.get_paragraphs())

   def paragraph_continuity(self, n_words = 10, generalized = False):
      return self._item_continuity(self.essay.get_paragraphs(), n_words, generalized)

   
   def _continuity(self, item_list, n_words = 10, generalized = True, mean_func = hmean):
      conts = self._item_continuity(item_list, n_words, generalized)
      return mean_func([x for x in conts if x > 0])
      
   def continuity(self, n_words = 10, generalized = True, mean_func = hmean,
                  paragraphs = False):
      if paragraphs:
         return self._continuity(eslf.essay.get_paragraphs(), n_words, generalized)
      else:    
         return self._continuity(self.essay.get_sentences(), n_words, generalized)


def test_topic_analysis(ungraded_dataset):
   test_essay(ungraded_dataset[0])

def test_essay(essay):
   
   def reorder_essay(essay, i = 1, j = 3):
      ps = essay.get_paragraphs()      
      ps[i], ps[j] = ps[j], ps[i]
      return Ungraded_Essays(paragraphs = ps)

   print("\n")
   def mean_test(ta, name, func):
      print(name)
      cont = ta.continuity(mean_func = func)
      gcont = ta.continuity(generalized = True, mean_func = func)
      print("   Best Only Continuity   : {:.5f}".format(cont))
      print("   Generalized Continuity : {:.5f}".format(gcont))
      print("")
   
   print(" --- Original --- ")
   ta1 = TopicAnalysis(essay)
   mean_test(ta1, "Harmonic", hmean)

   print(" --- Shuffled --- ")
   ta2 = TopicAnalysis(reorder_essay(essay))
   mean_test(ta2, "Harmonic", hmean)
   

def print_topics_with_conts(item_name, topics, conts):
   
   for idx in range(len(topics)-1):
      topic_id, conf = topics[idx]
      cont = conts[idx]

      print("{} {:2d} | Topic #{:d} : {:.3f}".format(item_name, idx, topic_id, conf)) 
      print("   Continuity : {:.3f}".format(cont))
   

   topic_id, conf = topics[-1]
   print("{} {:2d} | Topic #{:d} : {:.3f}".format(item_name, idx+1, topic_id, conf)) 


