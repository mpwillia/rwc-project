
import scipy
import scipy.ndimage
import numpy as np
import math

from multiprocessing import Pool 

# Image Manipulation ----------------------------------------------------------
def img_to_grayscale(img):
   gray_img = []

   for row in img:
      gray_img.append([pix_mean(p) for p in row])
   
   return gray_img

def _row_to_dists(pix_row):
   return [mean_abs_dist(pix) for pix in pix_row]

def img_to_dists(img, processes = 4):
   proc_pool = Pool(processes = processes) 

   dist_img = proc_pool.map(_row_to_dists, img)

   proc_pool.close()
   proc_pool.join()

   return dist_img


# Comparison to Brightness ----------------------------------------------------
def euc_dist(pixel, brightness = None):
   if brightness is None: brightness = pix_mean(pixel)

   a = float(pixel[0]) - brightness
   b = float(pixel[1]) - brightness
   c = float(pixel[2]) - brightness

   return math.sqrt((a*a)+(b*b)+(c*c))

def sq_dist(pixel, brightness = None):
   if brightness is None: brightness = pix_mean(pixel)
   
   a = float(pixel[0]) - brightness
   b = float(pixel[1]) - brightness
   c = float(pixel[2]) - brightness

   return (a*a)+(b*b)+(c*c) 

def mean_abs_dist(pixel, brightness = None):
   if brightness is None: brightness = pix_mean(pixel)
   return (abs(float(pixel[0]) - brightness) + \
           abs(float(pixel[1]) - brightness) + \
           abs(float(pixel[2]) - brightness)) / 3.0


# Pixel Math ------------------------------------------------------------------
def pix_arr_mean(pixels):
   return mean(pix_mean(p) for p in pixels)

def pix_mean(pixel):
   return (float(pixel[0]) + float(pixel[1]) + float(pixel[2])) / 3.0


# Generic Math ----------------------------------------------------------------
def mean(values):
   s = 0
   n = 0
   for val in values:
      s += float(val)
      n += 1
   return 0.0 if n == 0 else s / float(n)


# Image Saving/Writing --------------------------------------------------------
def save_image(img, path):
   scipy.misc.imsave(path, img)   

def load_image(img_file, mode = 'RGB'):
   return scipy.ndimage.imread(img_file, mode = mode)


