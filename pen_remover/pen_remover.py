#!/usr/local/bin/python3

import argparse
import numpy as np
import sys
import os
import glob

from multiprocessing import Pool

import util

def main():
   args = parse_args()
   
   for img_dir in args.image_dir:
      fail = False
      if not os.path.isdir(img_dir):
         print("Given path '{}' is not a directory!".format(img_dir))
         fail = True

      if fail: return

   output_dir = "./pen_removed_output"
   
   if not os.path.exists(output_dir):
      os.mkdir(output_dir)
   

   for paper_dir in args.image_dir:
      process_dir(paper_dir, output_dir)

def process_dir(dir_path, output_dir):
   
   dir_name = os.path.basename(dir_path)
   
   paper_dir = os.path.join(output_dir, dir_name)

   if os.path.exists(paper_dir):
      print("Output dir already exists for '{}' - skipping".format(dir_name))
      return
      #raise Exception("output paper directory already exists!")
   else: 
      os.mkdir(paper_dir)
   
   img_files = glob.glob(os.path.join(dir_path, '*.png'))

   print("Processing {} Images for Paper Dir '{}'".format(len(img_files), dir_name))
   for img_file in img_files:
      img_name = os.path.basename(img_file)
      img = util.load_image(img_file)
      filt_img = filter_pen2(img)
      util.save_image(filt_img, os.path.join(paper_dir, img_name))
   print("")

def clean_speckle(raw_img, grid = 11, w_ratio = 0.85):
   
   filtered_img = []
   
   grid_shift = int((grid - 1) / 2.0)
   num_rows = len(raw_img)
   for row_idx in range(num_rows):
      filtered_row = []
      
      sys.stdout.write("\r {:5d} / {:5d}   {:.2%}".format(row_idx, num_rows, row_idx / float(num_rows)))
      sys.stdout.flush()

      row = raw_img[row_idx]

      num_cols = len(row)

      if row_idx <= grid_shift or row_idx >= num_rows - grid_shift:
         filtered_img.append(row)
         continue

      row_mean = util.pix_arr_mean(row)
      if row_mean < 225:
         #filtered_img.append([[255,0,0] for i in range(num_cols)])
         filtered_img.append(row)
         continue

      for col_idx in range(num_cols):
         
         pixel = raw_img[row_idx][col_idx]
         
         if col_idx <= grid_shift or col_idx >= num_cols - grid_shift:
            filtered_row.append(pixel)
            continue
         
         brightness = mean(pixel)
         if brightness > 128:
            filtered_row.append([255]*3)
            continue

         count_white = 0
         count_black = 0

         black_cutoff = ((grid*grid) - int(grid*grid*w_ratio)) + 1

         adj_pixels = []
         
         for grid_idx in range(grid):
            if count_black > black_cutoff:
               count_white = 0
               break

            idx = grid_idx - grid_shift
            
            if idx == 0: continue
            
            for p in raw_img[row_idx+idx][col_idx-grid_shift : col_idx+grid_shift+1]:
               pix_b = util.pix_mean(p)
               if pix_b > 128:
                  count_white += 1
               else:
                  count_black += 1

         if count_white > w_ratio * grid * grid:
            filtered_row.append([255]*3)
            #filtered_row.append([255,0,0])
         else:
            filtered_row.append(pixel)
      
      filtered_img.append(filtered_row)

   print("\n")
   return filtered_img

#def _clean_speckle(raw)

def filter_pen2(raw_img, threshold = 5, grid_size = (9,17), local_grid_size = (5,5), edge_width = 250, edge_height = 100):

   print("Making Dist Img")
   dist_img = util.img_to_dists(raw_img, processes = 4)

   filtered_img = []
   
   gsx = int((grid_size[0] - 1) / 2.0)
   gsy = int((grid_size[1] - 1) / 2.0)
   total_grid_pix = np.prod(grid_size)
   
   lgsx = int((local_grid_size[0] - 1) / 2.0)
   lgsy = int((local_grid_size[1] - 1) / 2.0)
   local_total_grid_pix = np.prod(local_grid_size)

   num_rows = len(raw_img)
   for row_idx in range(num_rows):
   
      filtered_row = []
      sys.stdout.write("\r {:5d} / {:5d}   {:.2%}".format(row_idx, num_rows, row_idx / float(num_rows)))
      sys.stdout.flush()

      row = raw_img[row_idx]
      num_cols = len(row)

      if row_idx <= edge_height or row_idx >= num_rows - edge_height:
         filtered_img.append([[255]*3 for i in range(num_cols)])
         continue

      for col_idx in range(num_cols):
         filtered_img

         if col_idx <= edge_width or col_idx >= num_cols - edge_width:
            filtered_row.append([255]*3)
            continue

         dist = dist_img[row_idx][col_idx]
         
         if dist > threshold:

            local_row = raw_img[row_idx][col_idx - gsx : col_idx + gsx]
            
            num_pen = 0
            local_pen = 0
            #local_brightness = []
            grid_pix = []
            local_pix = []
            for gx, gy in grid_idx_gen((col_idx, row_idx), grid_size, include_start = False):
               gp = raw_img[gy][gx]
               
               if dist_img[gy][gx] > threshold:
                  if abs(gx - col_idx) <= lgsx and abs(gy - row_idx) <= lgsy:
                     local_pen += 1 
                  num_pen += 1
               else:
                  if abs(gx - col_idx) <= lgsx and abs(gy - row_idx) <= lgsy:
                     local_pix.append(gp)
                  grid_pix.append(gp)
            
            if len(grid_pix) <= 0:
               filtered_row.append([255]*3)
               continue
            
            
            brightness = util.pix_arr_mean(grid_pix)
            local_brightness = util.pix_arr_mean(local_pix)
            #brightness = util.pix_mean(raw_img[row_idx][col_idx])
            
            if local_pen >= 0.9 * local_total_grid_pix and num_pen >= 0.75 * total_grid_pix:
               filtered_row.append([255]*3)
            elif brightness > 230:
               filtered_row.append([255]*3)
            elif len(local_pix) >= 3:
               filtered_row.append([int(local_brightness)]*3)
            elif len(grid_pix) >= 3:
               filtered_row.append([int(brightness)]*3)
            else:
               filtered_row.append([255]*3)
         

         else:
            pix_b = util.pix_mean(raw_img[row_idx][col_idx])
            
            if pix_b > 210:
               filtered_row.append([255]*3)
            elif pix_b < 30:
               filtered_row.append([0]*3)
            else: 
               filtered_row.append(raw_img[row_idx][col_idx])
      
      filtered_img.append(filtered_row)
   
   print("")
   return filtered_img



def filter_pen(raw_img, threshold = 5, grid_size = (9,3), bw_bound = 10, 
               grid_bw_bound = 50, edge_width = 225, edge_height = 100):
   
   filtered_img = []
   #grayscale_img = util.img_to_grayscale(raw_img)

   gsx = int((grid_size[0] - 1) / 2.0)
   gsy = int((grid_size[1] - 1) / 2.0)
   total_grid_pix = np.prod(grid_size)
   
   num_rows = len(raw_img)
   for row_idx in range(num_rows):
      filtered_row = []
      
      sys.stdout.write("\r {:5d} / {:5d}   {:.2%}".format(row_idx, num_rows, row_idx / float(num_rows)))
      sys.stdout.flush()

      row = raw_img[row_idx]
      num_cols = len(row)

      if row_idx <= gsy + edge_height or row_idx >= num_rows - (gsy + edge_height):
         filtered_img.append([[0]*3 for i in range(num_cols)])
         continue

      for col_idx in range(num_cols):
         
         pixel = raw_img[row_idx][col_idx]
         
         if col_idx <= gsx + edge_width or col_idx >= num_cols - (gsx + edge_width):
            filtered_row.append([255]*3)
            continue
         
         brightness = util.mean(pixel)
         if brightness > 255 - bw_bound:
            filtered_row.append([255]*3)
            continue

         if brightness < bw_bound:
            filtered_row.append([0]*3)
            continue

         count_white = 0
         count_black = 0
         count_pen = 0

         adj_pixels = []

         for gx, gy in grid_idx_gen((col_idx, row_idx), grid_size, include_start = False):
            gp = raw_img[gy][gx]
            
            if check_pen(gp) >= threshold:
               count_pen += 1
            else:
               gp_b = util.pix_mean(gp)
               adj_pixels.append(gp_b)

               if gp_b < 128:
                  count_black += 1
               else:
                  count_white += 1

         #for grid_idx in range(grid):
         #   idx = grid_idx - grid_shift
         #   
         #   if idx == 0: continue
         #   
         #   for p in raw_img[row_idx+idx][col_idx-grid_shift : col_idx+grid_shift+1]:
         #      if check_pen(p) < threshold:
         #         pix_b = util.pix_mean(p)
         #         adj_pixels.append(pix_b)
         #         
         #         if pix_b < 128:
         #            count_black += 1
         #         else:
         #            count_white += 1
         #      else:
         #         count_pen += 1
         
         if len(adj_pixels) > 0:
            brightness = util.mean(adj_pixels)

            if brightness > 255 - grid_bw_bound:
               filtered_row.append([255]*3)
               continue

            if brightness < grid_bw_bound:
               filtered_row.append([0]*3)
               continue

         g_ratio = 0.4
         #b_min_ratio = 3/9.0
         #b_min_ratio = 3/25.0
         #w_min_ratio = 5/9.0
         w_ratio = 0.6

         if check_pen(pixel) >= threshold:
            # target pixel is pen
            if count_white >= w_ratio * total_grid_pix or count_pen >= g_ratio * total_grid_pix:
               filtered_row.append([255]*3)
               continue
            else:
               #filtered_row.append([brightness]*3)
               filtered_row.append([util.pix_mean(pixel)]*3)
               continue

         #elif count_white >= w_ratio * total_grid_pix:
         #   filtered_row.append([255]*3)

         else:
            filtered_row.append(pixel)

      filtered_img.append(filtered_row)
   
   print("")
   return filtered_img


def grid_idx_gen(start_idx, grid_size, include_start = True):
   sx,sy = start_idx
   gx,gy = grid_size

   if gx % 2 == 0 or gy % 2 == 0:
      raise ValueError("Grid Size values must be odd values")

   gsx = int((gx - 1) / 2.0)
   gsy = int((gy - 1) / 2.0)
   
   
   for x in range(sx - gsx, sx + gsx):
      for y in range(sy - gsy, sy + gsy):
         if x == sx and y == sy and not include_start: continue

         yield (x,y)


def check_pen(pixel):
   return util.mean_abs_dist(pixel) 

   #if pixel[0] < pixel[2] and pixel[2] < pixel[1]:
   #   return util.pix_mean(pixel) / float(pixel[1])
   #   #return float(pixel[2]) / float(pixel[1])
   #else:
   #   return 0.0


def parse_args():
   """
   Retrieve the user-entered arguments for the program.
   """
   parser = argparse.ArgumentParser(description = 
   """Perform Voice-Face-Text (VFT) speaker identification.""")
   parser.add_argument("image_dir", metavar = "IMG_DIR", nargs = "+", help = 
   """the path of the SRT file to diarize""")
   return parser.parse_args()

if __name__ == "__main__":
   main()
