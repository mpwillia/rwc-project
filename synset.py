from nltk.corpus import wordnet as wn


def topic_sim(lda, t1_num, t2_num, max_words = 10):
    return topic_sim_path(lda.get_topic_words(t1_num,max_words),
                          lda.get_topic_words(t2_num,max_words))


# Topic similarity using wn.path_similarity:
# Return a score denoting how similar two word senses are, based on the
# shortest path that connects the senses in the is-a (hypernym/hypnoym)
# taxonomy. The score is in the range 0 to 1.
def topic_sim_path(t1,t2):
    similarity = 0.0
    count = 0
    for w1 in t1:
        w1_synsets = wn.synsets(w1)
        for w2 in t2:
            w2_synsets = wn.synsets(w2)
            if len(w1_synsets) == 0 and len(w2_synsets) == 0:
                if w1 == w2:
                    similarity += 1.0
                count += 1
            elif len(w1_synsets) == 0:
                count += len(w2_synsets)
            elif len(w2_synsets) == 0:
                count += len(w1_synsets)
            else:
                for syn1 in w1_synsets:
                    for syn2 in w2_synsets:
                        sim = syn1.path_similarity(syn2)
                        count += 1
                        if sim is not None:
                            # Case where there is no common root
                            similarity += sim
                            # for no common root, similarity += 0
    if count == 0:
        return 0.0
    return similarity/count


# Topic similarity using wn.lch_similarity:
# Leacock-Chodorow Similarity: Return a score denoting how similar two word
# senses are, based on the shortest path that connects the senses (as above)
# and the maximum depth of the taxonomy in which the senses occur. The
# relationship is given as -log(p/2d) where p is the shortest path length and
# d the taxonomy depth.
# LCH requires same part of speech
def topic_sim_lch(t1,t2):
    similarity = 0.0
    count = 0
    for w1 in t1:
        w1_synsets = wn.synsets(w1)
        for w2 in t2:
            w2_synsets = wn.synsets(w2)
            if len(w1_synsets) == 0 and len(w2_synsets) == 0:
                if w1 == w2:
                    similarity += 1.0
                count += 1
            elif len(w1_synsets) == 0:
                count += len(w2_synsets)
            elif len(w2_synsets) == 0:
                count += len(w1_synsets)
            else:
                for syn1 in w1_synsets:
                    for syn2 in w2_synsets:
                        if syn1.pos() == syn2.pos():
                            sim = syn1.lch_similarity(syn2)
                        else:
                            sim = 0
                        count += 1
                        if sim is not None:
                            # Case where there is no common root
                            similarity += sim
                            # for no common root, similarity += 0
    if count == 0:
        return 0.0
    return similarity/count


# Topic similarity using wn.wup_similarity:
# Wu-Palmer Similarity: Return a score denoting how similar two word senses
# are, based on the depth of the two senses in the taxonomy and that of their
# Least Common Subsumer (most specific ancestor node). Note that at this time t
# he scores given do _not_ always agree with those given by Pedersen's Perl
# implementation of Wordnet Similarity.
def topic_sim_wup(t1,t2):
    similarity = 0.0
    count = 0
    for w1 in t1:
        w1_synsets = wn.synsets(w1)
        for w2 in t2:
            w2_synsets = wn.synsets(w2)
            if len(w1_synsets) == 0 and len(w2_synsets) == 0:
                if w1 == w2:
                    similarity += 1.0
                count += 1
            elif len(w1_synsets) == 0:
                count += len(w2_synsets)
            elif len(w2_synsets) == 0:
                count += len(w1_synsets)
            else:
                for syn1 in w1_synsets:
                    for syn2 in w2_synsets:
                        sim = syn1.wup_similarity(syn2)
                        count += 1
                        if sim is not None:
                            # Case where there is no common root
                            similarity += sim
                            # for no common root, similarity += 0
    if count == 0:
        return 0.0
    return similarity/count


# Given 2 topic wordlists, will report the average similarity score.
def topic_sim(t1,t2,type="path"):
    if type == "wup":
        return topic_sim_wup(t1,t2)
    elif type == "lch":
        return topic_sim_lch(t1,t2)
    else: # defaults to path_similarity
        return topic_sim_path(t1,t2)



#print("MOUSE TEST TIME")
#wordlist1 = ["state","cruz"]
#wordlist2 = ["cruz","pilot"]
#print(wordlist1)
#print(wordlist2)
#print("path, path by default, lch, wup")
#print(topic_sim(wordlist1,wordlist2))
#print(topic_sim(wordlist1,wordlist2,"path"))
#print(topic_sim_path(wordlist1,wordlist2))
#print(topic_sim_lch(wordlist1,wordlist2))
#print(topic_sim(wordlist1,wordlist2,"lch"))
#print(topic_sim_wup(wordlist1,wordlist2))
#print(topic_sim(wordlist1,wordlist2,"wup"))
#print("same list path")
#print(topic_sim_path(['state'],['state']))
#print(topic_sim(['state'],['state']))
#print(topic_sim(['state'],['state'],"lch"))
#print(topic_sim(['state'],['state'],"wup"))
#print(topic_sim([],[]))
#print(topic_sim([],[],"lch"))
#print(topic_sim([],[],"wup"))

