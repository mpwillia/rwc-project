# Validation Utilities
#
# Authors:
#   Michael Williams 
#   Alanna Buss 
#

from multiprocessing import Pool
from functools import partial
import util

# Classifier Validation -------------------------------------------------------

def _run_test(fold, classifier, kwargs):
   fold_num, train, test = fold
   clf = classifier(**kwargs)
   clf.train(train)
   return util.rmse(clf.test(test))

def validate_classifier(dataset, validation_func, classifier, kwargs, 
                        processes = 1):

   if processes <= 1:
      results = []
      for fold in validation_func(dataset):
         results.append(_run_test(fold, classifier, args, kwargs))
   else:
      proc_pool = Pool(processes = processes)
      
      val_func = partial(run_test, classifier = classifier, kwargs = kwargs)
      results = proc_pool.map(val_func, validation_func(dataset))
      proc_pool.close()
      proc_pool.join()
      
   return util.mean(results), util.stddev(results)


# Exhaustive Validation -------------------------------------------------------
def hold_one_out_validation(dataset):
   """
   Generator for producing folds of a dataset where each fold has only one
   element in it

   Arguments:
      |dataset| the dataset to produce folds from

   Yields the fold number, the training set outside the fold and the testing set 
      that is the fold. Eg: Yields (fold_number, training_set, testing_set)
   """
   return hold_n_out_validation(dataset, n = 1)

def hold_n_out_validation(dataset, n = 1):
   """
   Generator for producing folds of a dataset where each fold has approximately
   the given number of items.

   Arguments:
      |dataset| the dataset to produce folds from

   Optional:
      |n| the size of folds to produce, cannot be less than one and is capped
        at the size of the dataset given.

   Yields the fold number, the training set outside the fold and the testing set 
      that is the fold. Eg: Yields (fold_number, training_set, testing_set)
   """
   # k must be greater than one and is capped at the size of the dataset
   if n < 1: raise ValueError("n cannot be less than one")
   if n > len(dataset): n = len(dataset)
   
   return kfold_validation(dataset, k = len(dataset) // n)

# Non-Exhaustive Validation ---------------------------------------------------
def kfold_validation(dataset, k = 10):
   """
   Generator for producing folds of a dataset.

   The last fold produced by this generator may be a different size from the rest
   to ensure complete fold coverage.

   Arguments:
      |dataset| the dataset to produce folds from

   Optional:
      |k| the number of folds to produce, cannot be less than one and is capped
        at the size of the dataset given.

   Yields the fold number, the training set outside the fold and the testing set 
      that is the fold. Eg: Yields (fold_number, training_set, testing_set)
   """
   
   # k must be greater than one and is capped at the size of the dataset
   if k < 1: raise ValueError("k cannot be less than one")
   if k > len(dataset): k = len(dataset)

   fold_size = len(dataset) // k

   # Every fold except for the last is processed the same
   for fold_num in range(k-1):
      fold_idx = fold_num * fold_size
      test = dataset[fold_idx : fold_idx + fold_size]
      train = dataset[:fold_idx] + dataset[fold_idx + fold_size:]
      yield fold_num, train, test

   # The last fold is processed differently to ensure full coverage
   last_fold = len(dataset) - fold_size * (k - 1)
   yield k-1, dataset[:-last_fold], dataset[-last_fold:]




