# A Useful Timer Class
# Digital Democracy Project
# Institute for Advanced Technology and Public Policy
# California Polytechnic State University
#
# Authors:
#   Michael Williams (mpwillia@calpoly.edu)
#
# Advisor:
#   Foaad Khosmood (foaad@calpoly.edu)

import time
import datetime
from collections import deque
from scipy.stats import mstats
import numpy as np


"""
Basic Timer Class

Usage Example:
--------------

timer = Timer(start_now = True)

msg_fmt = "Processing Task {:d} / {:d}  {}"
for task_num, task in enumerate(task_list):
   sys.stdout.write(msg_fmt.format(task_num, len(task_list), timer.make_str()))
   sys.stdout.flush()
   process_task(task)
   timer.lap()

timer.stop() 
print("Finished Processing {:d} Tasks  {}".format(len(task_list), timer.make_str()))

"""


LAP_HISTORY_KEYWORDS = {'large': 25,
                        'stable': 25,
                        'small': 10,
                        'unstable': 10}

class Timer():
   
   def __init__(self, start_now = False, lap_history_size = None):
      """
      Constructor for Timer
      
      Optional:
         |start_now| if True, starts the timer immediately upon creation
         |lap_history_size| the size of the lap history to maintain. Used when
            reporting the average lap time and when making estimates about
            time remaining. Defaults to None.
            
            Can be an integer value or one of the following keywords:
               'large'
               'small'
               'stable'
               'unstable'

            If None the lap history size is unbounded.

            Larger vs Smaller Lap Histories:
               Larger lap histories respond slowly to changes in lap duration.
                  This means a larger lap history will more accurately report 
                  average lap time and estimated time remaining given noisy but 
                  overall stable lap times. Larger lap histories can be thought
                  of as given a better overall view of the lap times.
               Shorter lap histories respond faster to changes in lap duration.
                  This means a shorter lap history will more accurately report
                  average lap time and estimated time remaining given unstable
                  lap times. Shorter lap histories can be thought of as given a
                  better 'in the moment' view of the lap times.

      """
      
      if lap_history_size is None:
         self.lap_history_size = lap_history_size
      else:
         try:
            lap_history_size = int(lap_history_size)
         except ValueError:
            try:
               lap_history_size = LAP_HISTORY_KEYWORDS[lap_history_size]
            except KeyError:
               raise ValueError("Invalid lap history size!")

         if lap_history_size <= 0:
            raise ValueError("Invalid lap history size!")

         self.lap_history_size = lap_history_size
               
         
      self.start_time = None   # start time is a floating point in seconds
      self.end_time = None     # end time is a floating point in seconds

      self.laps = 0
      self.lap_start = None
      
      self.lap_history = []
     
      if start_now:
         self.start()

   # Timer History Functions --------------------------------------------------
   def _add_to_history(self, time):
      if self.lap_history_size is not None:
         if len(self.lap_history) > self.lap_history_size:
            self.lap_history.pop()
      self.lap_history.append(time)

   # Timer Action Functions ---------------------------------------------------
   def start(self):
      """ 
      Starts the timer.
      If the timer was already started, it will be reset completely.
      """
      self.running = True
      self.start_time = time.time()
      self.lap_start = self.start_time
      self.end_time = None
      self.laps = 0
      self.lap_history = []

   def stop(self):
      """
      Stops the timer, returning the elapsed time.
      If the timer was never start it returns None.
      If the timer has already been stopped this is equivalent to elapsed()
      """
      # set the end time if we are running
      if self.running:
         self.end_time = time.time()
         self.running = False

      return self.elapsed()

   def lap(self):
      """
      Marks the current time as a lap.
      
      Returns the lap time.
         If the timer was never started it returns None
         If the timer has been stopped it returns None
      """
      # if the timer isn't running don't do anything
      if not self.running: return None
            
      lap_end = time.time()
      lap_time = lap_end - self.lap_start
      self.lap_start = lap_end
      self.laps += 1
      self._add_to_history(lap_time)
      return lap_time
 
   def lap_multiple(self, laps):
      """
      Marks the current time as the end of multiple laps. Divides the total lap
      time evenly between the number of laps given. Increments the total lap
      count and adds one value into the lap history for each of the given number 
      of laps.

      This is useful when working with concurrency. For example if you have a
      process monitoring the progress of a task queue, multiple tasks could be
      completed between probes of the progress.

      Arguments
         |laps| the number of laps to mark, must be >= 1

      Returns the lap time for the last of the 
         If the timer was never started it returns None
         If the timer has been stopped it returns None
      """
      if laps == 1: self.lap()
      elif laps < 1:
         raise ValueError("Given laps to mark must be at least 1!")

      # if the timer isn't running don't do anything
      if not self.running: return None
                  
      lap_end = time.time()
      sub_lap_time = lap_end - self.lap_start
      sub_lap_time = total_lap_time / float(laps)
      
      for _ in range(laps):
         self._add_to_history(sub_lap_time)
         self.laps += 1

      self.lap_start = lap_end
      return sub_lap_time  

   # Timer Info Functions -----------------------------------------------------
   def elapsed(self):
      """
      Gets the total elapsed time the timer has been running for.
      If the timer has never been started, returns None
      If the timer has been started and is still running, returns the elapsed
         time between the start time and the time this function call was made.
      If the timer has been stopped, returns the elapsed time from the start time
         to the end time.
      """
      # check if we've never been started
      if self.start_time is None: return None
      
      # if we are currently running then return how long we've been running
      if self.running:
         # currently running
         return time.time() - self.start_time
      else:
         # timer stopped
         return self.end_time - self.start_time
   
   def avg_lap_time(self):
      """
      Gets the average lap time so far.
      If no laps have been marked, returns None
      If at least one lap has been marked by calling lap() or stop() then this
         function returns the 10% winsorized mean of the lap history.
      """
      if len(self.lap_history) <= 0: return None
      
      # compute the 10% winsorized mean
      winsorized_history = mstats.winsorize(self.lap_history, limits = (0.1, 0.1))
      return np.mean(winsorized_history)

   # Timer String Functions ---------------------------------------------------
   def est_time_str(self, left):
      """
      Gets the estimated time remaining as a formatted string.

      Arguments:
         |left| the number of laps left.

      Returns a formatted string reporting the estimated time remaining for the
         given number of laps. If the timer hasn't been started or no laps have
         been marked then this will return a string reporting so instead.
      """
      avg = self.avg_lap_time()
      if avg is None:
         #return "Timer not started or no laps have been marked!"
         return "" 
      else:
         est = avg * left
         return "[Est Time For {0:d} More Laps: {1}]".format(left, time_str(est))

   def avg_time_str(self, laps = 1):
      """
      Gets the average time per the given number of laps as a formatted string.
      
      Optional:
         |laps| the number of laps to report the average time over. For example,
            a value of 5 would report the average time it takes for 5 laps.
            Defaults to 1.
      
      Returns a formatted string reporting the average time to complete the given
         number of laps. If the timer hasn't been started or no laps have been 
         marked then this will return a string reporting so instead.
      """
      avg = self.avg_lap_time()
      if avg is None:
         #return "Timer not started or no laps have been marked!"
         return ""
      elif laps > 1:
         return "[Avg Time For {0:d} Laps: {1}]".format(laps, time_str(avg * laps))
      else:
         return "[Avg Time Per Lap: {1}]".format(laps, time_str(avg))

   def elapsed_time_str(self):
      """
      Gets the elapsed time as a formatted string.

      Returns a formatted string reporting the elapsed time so far. If the timer
         hasn't been started then a string reporting so will be returned instead.
      """
      dur =  self.elapsed()
      if dur is None:
         return "Timer not started!"
      else:
         return "[Elapsed Time: {0}]".format(time_str(dur))
   
   def make_str(self, elapsed = True, avg = True, est = None):
      """
      Creates a formatted string containing the information specified.

      Optional:
         |elapsed| if True then elapsed time information will be included
         |avg| if True then average time per lap information will be included
         |est| if not None and an integer, then will include the estimated time 
            to completed the given number of laps. For example a value of 5 will
            report the estimated time to complete 5 laps.

      Returns a formatted string based on the information specified.
      """
      comps = []
      if elapsed: comps.append(self.elapsed_time_str())
      if avg: comps.append(self.avg_time_str())
      if est is not None: comps.append(self.est_time_str(est))
      if len(comps) > 0:
         return "  ".join(comps)
      else:
         return ""


   def __str__(self):
      return self.elapsed_time_str()
      
   def __repr__(self):
      return self.__str__()

def time_str(time_sec):
   milliseconds = int((time_sec * 1000.0) % 1000)
   seconds = int(time_sec % 60)
   minutes = int((time_sec / 60) % 60)
   hours = int((time_sec / (60 * 60)))
   return "{0:02d}:{1:02d}:{2:02d}.{3:03d}".format(hours, minutes, seconds, milliseconds)


def timer_tests():
   timer = Timer(start_immediately = True)
   iters = 10
   for i in range(iters):
      time.sleep(2)
      timer.lap()
      print(timer.make_str(est = iters - (i+1)))
   timer.stop()
   print(timer.make_str())

if __name__ == "__main__":
   timer_tests()

