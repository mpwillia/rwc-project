# Author: Olivier Grisel <olivier.grisel@ensta.org>
#         Lars Buitinck
#         Chyi-Kwei Yau <chyikwei.yau@gmail.com>
# License: BSD 3 clause

from __future__ import print_function
from time import time

from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation
from sklearn.datasets import fetch_20newsgroups
import scipy

n_samples = 2500
n_features = 1000
n_topics = 10
n_top_words = 15


#def print_top_words(model, feature_names, n_top_words):
#    for topic_idx, topic in enumerate(model.components_):
#        print("Topic #%d:" % topic_idx)
#        print(" ".join([feature_names[i]
#                        for i in topic.argsort()[:-n_top_words - 1:-1]]))
#    print()


def print_top_words(model, feature_names, n_top_words):
    for topic_id, topic in enumerate(model.components_):
        print('\nTopic #%d:' % int(topic_id + 1)) 
        if n_top_words is None:
            print(''.join([feature_names[i] + ' ' + str(round(topic[i], 2))
                  +' | ' for i in range(len(topic))]))
        else:
            print(''.join([feature_names[i] + ' ' + str(round(topic[i], 2))
                  +' | ' for i in topic.argsort()[:-n_top_words - 1:-1]]))
            #print(''.join([feature_names[i] + ' ' + str(round(topic[i], 2))
            #      +' | ' for i in range(len(topic[:n_top_words - 1]))]))

# Load the 20 newsgroups dataset and vectorize it. We use a few heuristics
# to filter out useless terms early on: the posts are stripped of headers,
# footers and quoted replies, and common English words, words occurring in
# only one document or in at least 95% of the documents are removed.

print("Loading dataset...")
t0 = time()
dataset = fetch_20newsgroups(shuffle=True, random_state=1,
                             remove=('headers', 'footers', 'quotes'))
data_samples = dataset.data[:n_samples]

#data_samples = ["I ate a banana and spinach smoothie for breakfast",
#                "I like to eat broccoli and bananas.",
#                "Chinchillas and kittens are cute.",
#                "My sister adopted a kitten yesterday.",
#                "My sister adopted two kittens yesterday.",
#                "I love kittens.",
#                "Look at this cute hamster munching on a piece of broccoli.",
#                "Who doesn't love bananas?",
#                "Lets eat kittens for breakfast!",
#                "What! No, eat broccoli instead!"]

print("done in %0.3fs." % (time() - t0))
print(data_samples[0])
print("")

# Feature Extraction ----------------------------------------------------------

# Use tf-idf features for NMF.
print("Extracting tf-idf features for NMF...")
tfidf_vectorizer = TfidfVectorizer(max_df=0.95, min_df=2,
                                   max_features=n_features,
                                   stop_words='english')
t0 = time()
tfidf = tfidf_vectorizer.fit_transform(data_samples)
print("done in %0.3fs." % (time() - t0))

# Use tf (raw term count) features for LDA.
print("Extracting tf features for LDA...")
tf_vectorizer = CountVectorizer(max_df=0.95, min_df=2,
                                max_features=n_features,
                                stop_words='english')
t0 = time()
tf = tf_vectorizer.fit_transform(data_samples)
print("done in %0.3fs." % (time() - t0))

print("")

# Training Models -------------------------------------------------------------

# Fit the NMF model
#print("Fitting the NMF model with tf-idf features, "
#      "n_samples=%d and n_features=%d..."
#      % (n_samples, n_features))
#t0 = time()
#nmf = NMF(n_components=n_topics, random_state=1,
#          alpha=.1, l1_ratio=.5).fit(tfidf)
#print("done in %0.3fs." % (time() - t0))
#
#print("\nTopics in NMF model:")
#tfidf_feature_names = tfidf_vectorizer.get_feature_names()
#print_top_words(nmf, tfidf_feature_names, n_top_words)


print("Fitting LDA models with tf features, "
      "n_samples=%d and n_features=%d..."
      % (n_samples, n_features))
lda = LatentDirichletAllocation(n_topics=n_topics, max_iter=5,
                                learning_method='online',
                                learning_offset=50.,
                                random_state=0)
t0 = time()
lda.fit(tfidf)
print("done in %0.3fs." % (time() - t0))

print("\nTopics in LDA model:")
tf_feature_names = tfidf_vectorizer.get_feature_names()
print_top_words(lda, tf_feature_names, n_top_words)

def find_topic_similarity(model, top_n = 5):
    print("\nLDA Topic Similarity")
    topic_sims = []
    for topic_id_a, topic_a in enumerate(model.components_):
        for topic_id_b, topic_b in enumerate(model.components_):
            if topic_id_a == topic_id_b: continue

            dist = scipy.spatial.distance.cosine(topic_a, topic_b)
            #dist = scipy.spatial.distance.correlation(topic_a, topic_b)
            
            topic_sims.append((topic_id_a+1, topic_id_b+1, dist))

            #print("Topic #{:d} vs Topic #{:d} : {:.3f}".format(topic_id_a, topic_id_b, dist))
    
    
    sorted_sims = sorted(topic_sims, key = lambda x: x[2])
    
    print("Top {:d} Most Similar Topics".format(top_n))
    for sims in sorted_sims[:top_n]:
        #id_a, id_b, dist = sims
        print("  Topic #{:d} vs Topic #{:d} : {:.3f}".format(*sims))
    
    print("")
    print("Top {:d} Least Similar Topics".format(top_n))
    for sims in sorted_sims[-top_n:]:
        #id_a, id_b, dist = sims
        print("  Topic #{:d} vs Topic #{:d} : {:.3f}".format(*sims))

    print("\n")

find_topic_similarity(lda)

print("\nSentence Example")
test_sents = ["I really love broccoli so much because broccoli is absolutely broccoli.",
              "I really love cute so much because cute is absolutely banana.",
              "Kittens are the best things ever!"]

tf = tf_vectorizer.transform(test_sents)
results = lda.transform(tf)

for sent, result in zip(test_sents, results):
    print("'{}'".format(sent))
    for topic_p, comp in zip(result, enumerate(lda.components_)):
        topic_id, topic = comp
        print("Topic #{:d}:  {:.3f}".format(topic_id+1, topic_p)) 
    print("\n")    

print(lda.components_)

