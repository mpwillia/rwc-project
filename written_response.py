def written_response(graded_essay_features, grade, baseline_features):
   functions = {'avg_sent_per_par' : sent_per_par, 
                  'avg_vocab_per_par' : avg_vocab_per_par}
   grade_hierarchy = ['A', 'B', 'C', 'D', 'F']
   intensity = 0
   if grade == 'Fail':
      intensity = 3
   elif grade == 'Pass':
      intensity = 1
   else:
      intensity = grade_hierarchy.index(grade)

   written_responses = {}
   written_responses['Introduction'] = introduction(grade)

   for feature, value in graded_essay_features.items():
      target_avg = 0
      if grade == 'Pass' or grade == 'Fail':
         target = baseline_features['Pass'][feature]
      else:
         target = baseline_features['A'][feature]
      try:
         written_responses[feature] = functions[feature](value, intensity, target)
      except KeyError:
         continue

   return written_responses

def sent_per_par(graded_essay_value, intensity, target):

   text = "Problem."
   no_improvements = abs(target[0] - graded_essay_value) < target[1] 
   many_improvements = abs(target[0] - graded_essay_value) > target[1] * 2
   lower = target[0] - graded_essay_value
   change = ""
   changing = ""

   if lower < 0:
      change = "decrease"
      changing = "decreasing"
   else:
      change = "increase"
      changing = "increasing"

   if intensity == 0:
      if no_improvements:
         text ="Your paragraph size looks okay."
      elif many_improvements:
         text = "According to my data set you might think about {} ".format(changing) + \
               "the amount of information you include."
      else:
         text = "Your paper might get even better if you {} ".format(change) + \
               "the length of your paragraphs."

   elif intensity == 1:
      if no_improvements:
         text ="Your paragraph size looks okay."
      elif many_improvements:
         text = "You could improve your paper a little by {} ".format(changing) + \
               "the length of your paragraphs."
      else:
         text = "You could consider {} the length of your ".format(changing) + \
               "paragraphs to improve your paper."

   elif intensity == 2:
      if no_improvements:
         text ="Your paragraph size looks okay."
      elif many_improvements:
            text = "You should {} the length of your ".format(change) + \
                  "paragraphs to improve your paper."
      else:
            text = "Your paper would benefit from a {} in paragraph length.".format(change)

   elif intensity == 3:
      if no_improvements:
         text ="Your paragraph size looks okay."
      elif many_improvements:
         text = "You need to {} the length of your paragraphs.".format(change)
      else:
         text = "Your paper's paragraphs need to be {} in length.".format(change + "d")

   else:
      if no_improvements:
         text ="Your paragraph size looks okay."
      elif many_improvements:
         text = "If you don't {} the length of your paragraphs you ".format(change) + \
               "will not pass."
      else:
         text = "You desperately need to {} the length of your paragraphs.".format(change)


   return text

def avg_vocab_per_par(graded_essay_value, intensity, target):
   text = "problem"
   
   no_improvements = abs(target[0] - graded_essay_value) < target[1] 
   many_improvements = abs(target[0] - graded_essay_value) > target[1] * 2
   lower = target[0] - graded_essay_value
   change = ""
   changing = ""

   if lower < 0:
      change = "decrease"
      changing = "decreasing"
   else:
      change = "increase"
      changing = "increasing"

   if intensity == 0:
      if no_improvements:
         text = "Your vocabulary usage looks okay."
      elif many_improvements:
         text = "According to my data set you might think about {} ".format(changing) + \
               "the amount of vocabulary you include per paragraph."
      else:
         text = "Your paper might get even better if you {} ".format(change) + \
               "the amount of vocabulary you use."

   elif intensity == 1:
      if no_improvements:
         text = "Your vocabulary usage looks okay."
      elif many_improvements:
         text = "You could improve your paper a little by {} ".format(changing) + \
               "the diversity of your words."
      else:
         text = "You could consider {} the vocablary in your ".format(changing) + \
               "paragraphs to improve your paper."

   elif intensity == 2:
      if no_improvements:
         text = "Your vocabulary usage looks okay."
      elif many_improvements:
            text = "You should {} the vocabulary in your ".format(change) + \
                  "paragraphs to improve your paper."
      else:
            text = "Your paper would benefit from a {} in vocabulary.".format(change)

   elif intensity == 3:
      if no_improvements:
         text = "Your vocabulary usage looks okay."
      elif many_improvements:
         text = "You need to {} the vocabulary in your paragraphs.".format(change)
      else:
         text = "Your paper's vocabulary need to be {}.".format(change + "d")

   else:
      if no_improvements:
         text = "Your vocabulary usage looks okay."
      elif many_improvements:
         text = "If you don't {} the vocabulary in your paragraphs you ".format(change) + \
               "will not pass."
      else:
         text = "You desperately need to {} the vocabulary you use.".format(change)


   return text

def introduction(grade):
   text = ""
   if grade == 'A':
      text = "This paper would statistically get an A. Here are some thoughts " + \
            "or suggesetions you might consider to further improve your paper."
   elif grade == 'B' or grade == 'Pass':
      text = "This paper would statistically get a {}. The ".format(grade) + \
               "are some suggestions that you may consider to improve your paper."
   elif grade == 'C':
      text = "This paper would statistically get a C. The following are some " + \
            "suggestions on how to improve."
   elif grade == 'D' or grade == 'Fail':
      text = "This paper would statistically get a {}. Here are ".format(grade) + \
            "suggestions on how to improve your paper based off these statistics."
   else:
      text = "This paper would statistically get an F. Based on the following " + \
            "features and the data collected on them. Following each feature is " + \
            "a suggestion on how to get closer to a statistical A."

   return text
