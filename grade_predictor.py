import nltk
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.util import ngrams, everygrams
from nltk.classify import maxent, naivebayes
import numpy

class Grade_Predictor():

   def __init__(self, binning = True, use_a_to_f = True, use_known_best = True):
      """
      Inits the grade predictor with whether or not it should be binning the
         grades or not.
      Arguments:
         |binning| a boolean on whether or not the grades should be binned.
      """
      self.bins = binning
      #True means A - F anything else means Pass/Fail
      self.bin_type = use_a_to_f

      if use_known_best:
         if self.bin_type:
            self.features = {'vocab' : True, 'sentence_per_par' : False, 
                              'word_diversity' : False, 'word_per_sentence' : False, 
                              'char_per_word' : False}
         else:
            self.features = {'vocab' : True, 'sentence_per_par' : True, 
                              'word_diversity' : False, 'word_per_sentence' : False, 
                              'char_per_word' : False}
      else:
         # features in order:
         # vocab, sentece / par, word/sent, word diversity /par, char / word
         self.features = {'vocab' : True, 'sentence_per_par' : True, 
                           'word_diversity' : False, 'word_per_sentence' : False, 
                           'char_per_word' : False}

      #classifier -0 is NaiveBayes Classifier 1 is Maxent
      self.classifier_type = 0
      self.classifier = None
      self.grade_statistics = None

   def __getstate__(self):
      return self.bins, self.bin_type, self.features, self.classifier_type, self.classifier, self.grade_statistics

   def __setstate__(self, state):
      self.bins, self.bin_type, self.features, self.classifier_type, self.classifier, self.grade_statistics = state

   def create_data_set(self, essays):
      """
      Makes a data set touple for the classifier.

      Arguments:
         |essays| The essays to Graded_essays to train on.

      Returns:
         A list of touples that follows the format (essay, overall grade)
         Where the overall grade is the average of the domain1 overall and 
         domain2 overall. If there is None for either of the overalls it is 
         not counted and the overall grade will only reflect the grade that was
         given. If both grades are none then it won't be added to the dataset.
      """
      training_data = []
   
      for essay in essays: 
         written = essay.essay
         grade1 = essay.overall_domain1
         grade2 = essay.overall_domain2
         grade_overall = 0
         essay_set = essay.essay_set
         if len(word_tokenize(written)) == 0:
            continue
         if grade1 is '':
            if grade2 is '':
               print("Essay " + essay.essay_id + " in set " + essay.essay_set
                     + " has no grades and is being omitted.\n")
               continue
            else:
               grade_overall = grade2
         elif grade2 is None:
            grade_overall = grade1
         else:
            grade_overall = (grade1 + grade2)/2.0

         if self.bins:
            grade_overall = self.bin_grade(grade_overall)
         
         training_data.append((written, grade_overall))

      return training_data

   def get_grade_distribution(self, essays):
      grades = [grade for essay, grade in self.create_data_set(essays)]
      total = len(grades)
      grade_distribution = "Grade Distribution\n"
      for diff_grade in sorted(set(grades)):
         count = grades.count(diff_grade)
         percent = count / total
         grade_distribution += "{}: {} {:.2%}\n".format(diff_grade, count, percent)
      return grade_distribution

   def bin_grade(self, grade):
      """
      Bins the arguments into labels :)
      Arguments:
         |grade| the grade to be binned.
      """
      if self.bin_type:
         bounds = [.6, .7, .8, .9]
         labels = ["F", "D", "C", "B", "A"]
      else:
         bounds = [.7]
         labels = ["Fail", "Pass"]
      grade_label = labels[-1]
      for idx in range(len(bounds)):
         bound = bounds[idx]
         if grade < bound:
            grade_label = labels[idx]
            break

      return grade_label

   def create_feature_set(self, essay):   
      """
      Make the feature set for the given essay.

      Current features:
         average words per sentence features[2]
         vocabulary diversity per paragraph features[3]
         vocabulary per paragraph features[0]
         average sentences per paragraph features[1]
         average characters per word features[5]

      Unused features:
         
         Num Sentences
         Entire essays
         Num Words
         Num characters
      """
      feature_set = dict()

      par_essays = essay.split("\n")
      
      #Entire Essays
      #feature_set["Essay"] = essay
      
      #Num Sentences
      #feature_set['num_sentences'] = len(sent_tokenize(essay))
      
      #Num Words 
      #feature_set['num_words'] = len(word_tokenize(essay))
      
      #Num Characters
      #feature_set['num_char'] = [len(word) for word in word_tokenize(essay)]
      
      #sent/par 
      if self.features['sentence_per_par']:
         feature_set['avg_sent_per_par'] = numpy.mean([len(sent_tokenize(par))
                                                   for par in par_essays])
      
      #word/sent
      if self.features['word_per_sentence']:
         feature_set['avg_word_per_sent'] = numpy.mean([len(word_tokenize(sent)) for
                                                   sent in sent_tokenize(essay)])
      
      #char/word
      if self.features['char_per_word']:
         feature_set['avg_char_per_word'] = numpy.mean([len(word) for word in
                                          word_tokenize(essay)])

      #vocab/par
      if self.features['vocab']:
         feature_set['avg_vocab_per_par'] = numpy.mean([len(set(word_tokenize(par)))
                                          for par in par_essays])

      #word diversity / par 
      if self.features['word_diversity']:
         feature_set['avg_word_diversity_per_par'] = numpy.mean([len(set(word_tokenize(par))) /
                                                 len(word_tokenize(par)) for par in
                                                   par_essays])
      
      
      #Randomly really well working first letter features
      #feature_set['fl_word_per_sent'] = numpy.mean([len(word_tokenize(sent)) for
      #                                             sent in sent_tokenize(essay[0])])
      #feature_set['fl_char_per_word'] = numpy.mean([len(word) for word in
      #                                    word_tokenize(essay[0])])
      #feature_set['fl_vocab_per_par'] = len(set(word_tokenize(essay[0])))
      #feature_set['fl'] = essay[0]


      return feature_set


   def grade(self, essay):
      """
      Takes the essay and returns the grade given by the classifier.
      Arguments:
         |essay| the essay to grade.
      """
      feature_set = self.create_feature_set(essay)
      return self.classifier.classify(feature_set)

   def get_grade_statistics(self, test_essays):
      """
      Takes a list of Graded_essays and prints the averages the feature set per
         grade level. 
      """
      test_data = self.create_data_set(test_essays)
      grade_dict = {}
      for essay, grade in test_data:
         feature_set = self.create_feature_set(essay)
         try:
            grade_dict[grade].append(feature_set)
         except KeyError:
            grade_dict[grade] = [feature_set]
      stat_dict = {}
      for grade, feature_sets in grade_dict.items():
         holding_dict = {}
         for feature_set in feature_sets:
            for feature, num in feature_set.items():
               try:
                  holding_dict[feature].append(num)
               except KeyError:
                  holding_dict[feature] = [num]
         for feature in holding_dict.keys():
            holding_dict[feature] = (numpy.mean(holding_dict[feature]), 
                                    numpy.std(holding_dict[feature]))
         stat_dict[grade] = holding_dict

      return stat_dict


   def test(self, test_essays):
      """
      Takes a list of Graded_essays and returns a list of touples where the 
         first item is the expected output and the second item is what the 
         classifier returned for that output.
      Arguments:
         |test_essays| the essays to be tested
      Returns     return grade_distribution:
         A list of touples where the first item is the expected output and the
            second item is what the classifier returned for that output.
      """
      pred = []
      test_data = self.create_data_set(test_essays)
      for essay, grade in test_data:
         pred.append((grade, self.grade(essay)))
      print(self.classifier.show_most_informative_features(30))
      return pred

   def binned_accuracy(self, pred):
      """
      Takes in the list of predictions and returns a string with
         the binned accuracy of the predictions.
      """
      grade_dict = {'A': 1, 'B':2, 'C':3, 'D':4, 'F':5}
      binned_pred = [abs(grade_dict[expected] - grade_dict[given]) 
                     for expected, given in pred]

      correct = binned_pred.count(0) / len(binned_pred)
      one_away = binned_pred.count(1) / len(binned_pred)  
      two_away = binned_pred.count(2) / len(binned_pred)  
      three_away = binned_pred.count(3) / len(binned_pred)  
      four_away = binned_pred.count(4) / len(binned_pred)  

      return ("Accuracies:\nCorrect Grade: {:.2%}\nOne Grade Away: {:.2%}\n"
               "Two Grades Away: {:.2%}\nThree Grades Away: {:.2%}\n"
               "Four Grades Away: {:.2%}\n".format(correct, one_away, two_away,
               three_away, four_away))

   def train(self, training_essays):
      """
      Trains the classifier on the given essays.
      Current Classifier: NaiveBayes
      Commented out Classifiers: Maxent
      Arguments:
         |training_essays| the essays to train the classifier on.
      """
      training_data = self.create_data_set(training_essays)
      training_feature_set = [(self.create_feature_set(essay), grade) for
                              essay, grade in training_data]
      
      if self.classifier_type == 1:
         self.classifier = maxent.MaxentClassifier.train(training_feature_set, 
                      max_iter = 100, min_lldelta = 0.001)
      elif self.classifier_type == 0:
         self.classifier = naivebayes.NaiveBayesClassifier.train(training_feature_set)

      self.grade_statistics = self.get_grade_statistics(training_essays)



