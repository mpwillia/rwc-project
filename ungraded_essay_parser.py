from essays import Ungraded_Essays
from essays import Mistake
import re

def parse_ungraded_papers(filepath):
   ungraded_dataset = []
   with open(filepath, "r", errors = 'ignore') as essays:
      papers = essays.read().split("</DOC>")
      first_paper = True
      for paper in papers:
         nid = None
         title = None
         paragraphs = []
         teacher = None
         mistakes = []

         nid_regex = r'DOC nid=\"(\d+)?\"'
         nid_match = re.search(nid_regex, paper)
         if nid_match:
            nid = nid_match.group(1)
         
         title_regex = r'\<TITLE\>\n([A-Z a-z]+)?\n\<\/TITLE\>'
         title_match = re.search(title_regex, paper)
         if title_match:
            title = title_match.group(1)

         paragraph_regex = r'<P>\n(.+)?\n<\/P>'
         paragraphs = re.findall(paragraph_regex, paper)

         teacher_regex = r'ANNOTATION teacher_id=\"(\d+)?\"'
         teacher_match = re.search(teacher_regex, paper)
         if teacher_match:
            teacher = teacher_match.group(1)
      
         annotation_regex = (r'<MISTAKE start_par=\"(?P<start_par>\d+)\"'
         r' start_off=\"(?P<start_char>\d+)\" end_par=\"(?P<end_par>\d+)\"'
         r' end_off=\"(?P<end_char>\d+)\">\n<TYPE>(?P<error_type>.+)'
         r'<\/TYPE>\n<CORRECTION>(?P<correction>.+)?<\/CORRECTION>\n'
         r'(<COMMENT>(?P<comment>.+)<\/COMMENT>\n)?<\/MISTAKE>')

         mistake_iter = re.finditer(annotation_regex, paper)
         for match in mistake_iter:
            mistakes.append(Mistake(**match.groupdict()))

         ungraded_dataset.append(Ungraded_Essays(nid, title, paragraphs, teacher))
          
   return ungraded_dataset

   

if __name__ == '__main__':
   parse_ungraded_papers("corpora/Ungraded_Corpus/data/nucle3.2.sgml")

