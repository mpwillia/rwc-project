# Utility functions for the rwc
#
# Authors:
#  Alanna Buss
#  Nicole Martin
#  Michael Williams
#

import numpy

def prediction_accuracy(predictions):
   """
   Takes in a list of touples where the first is the expected output and
      the second is the program given output and gives an accuracy
   Arguments:
      |predictions| the predictions to be counted
   Returns:
      The accuracy of the predictions from 0 - 1 
   """
   accuracy_list = [1.0 if expected == given else 0.0
                     for expected, given in predictions]
   return numpy.mean(accuracy_list)

def str_to_int(x):
   try:
      return int(x.strip())
   except ValueError:
      None

def handle_kwargs(defaults, given):
   """
   Utility function for handling kwargs.
   If |given| contains keys not in |defaults| then they are ignored.

   Arguments:
      |defaults| the default dictionary of key word arguments.
      |given| the given dictionary of key word arguments. If None then |defaults|
         is simply returned.

   Returns the processed dictionary of key word arguments
   """
   if given is None:
      return defaults
   else:
      kwargs = dict()
      for k,v in defaults.items():
         try:
            kwargs[k] = given[k]
         except KeyError:
            kwargs[k] = v
      return kwargs

def print_stat_dict(stat_dict):
   """
      Prints a statistic dictionary in the form of dictionary dictionary
   """
   text = "Statistics:\n"
   for grade, feature_set in sorted(stat_dict.items(), key = lambda x : x[0]):
      text += grade + ":\n"
      for feature, num in feature_set.items():
         text += str(feature) + " : " + str(num) + "\n"
   print(text)






