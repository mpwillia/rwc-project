
from topic_analysis import TopicAnalysis
from essays import Ungraded_Essays
import random
import numpy as np

from multiprocessing import Pool 
from multi_proc import safe_dispatch, monitor_progress

from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import accuracy_score

def test_continuity(ungraded_dataset, processes = 4):
   
   seed = 3472

   print("Running Continuity Test on {} Items".format(len(ungraded_dataset)))

   #if processes > 1:
   #   print("Dispatching {} Processes".format(processes))
   #   pool = Pool(processes, proc_init, (seed,))
   #   results = safe_dispatch(pool, test_essay, ungraded_dataset)
   #else:
   #   results = [test_essay(essay) for essay in ungraded_dataset]
  
   results = monitor_progress(processes, test_essay, ungraded_dataset)

   preds = []
   for base_cont, mean_test_conts in results:
      preds.append(base_cont > mean_test_conts)
   
   truth = [True]*len(preds)
   acc = accuracy_score(truth, preds)
   results_score = precision_recall_fscore_support(truth, preds, 
                                                   average = 'binary', 
                                                   pos_label = True)
   
   print("Accuracy  : {:7.2%}".format(acc))
   print("Precision : {:.3f}".format(results_score[0]))
   print("Recall    : {:.3f}".format(results_score[1]))
   print("F Score   : {:.3f}".format(results_score[2]))
   
   print(results_score[3])

def proc_init(seed):
   random.seed(seed)

def test_essay(ungraded_essay):
   
   test_essays = shuffle_order(ungraded_essay)
   
   base_ta = TopicAnalysis(ungraded_essay)
   base_cont = base_ta.continuity()
   
   test_conts = []
   for test_essay in test_essays:
      #ta = TopicAnalysis(test_essay)
      #test_conts.append(ta.continuity())
      test_conts.append(base_ta._continuity(test_essay.get_sentences()))
   
   return (base_cont, np.mean(test_conts))

def shuffle_order(ungraded_essay, num_orders = 3):
   
   paras = ungraded_essay.get_paragraphs()

   if num_orders > len(paras)*2:
      raise ValueError("num orders must be less than or equal to the number of paragraphs")

   base_order = list(range(len(paras)))

   bad_orders = set([tuple(base_order), tuple(base_order[::-1])]) 
   orders = set()
   
   while len(orders) < num_orders:
      random.shuffle(base_order)
      orders.add(tuple(base_order))
   
   
   def load_order(paras, order):
      new_paras = [paras[i] for i in order]
      return Ungraded_Essays(paragraphs = new_paras)
   
   return [load_order(paras, order) for order in orders]

