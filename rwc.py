import written_response
import graded_essay_parser
import ungraded_essay_parser
import topic_analysis
import topic_tests
import os
from grade_predictor import Grade_Predictor
import util
import validation
import numpy as np
import argparse
import sys
import pickle

from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import accuracy_score


CORPORA_DIR = "./corpora"
GRADED_CORPUS_DIR = "hewlett-automated-essay-scoring"
GRADED_CORPUS_FILE = "training_set_rel3.tsv"

UNGRADED_CORPUS_DIR = "release3.2"
UNGRADED_CORPUS_FILE = "nucle3.2.sgml"

A_F_MODEL = "a_f_grade_predictor.pkl"
PASS_FAIL_MODEL = "pass_fail_grade_predictor.pkl"


def main():
   
   args = parse_args()


   if args.test_grade_predictor or args.test_continuity:
      ungraded_corpus = load_ungraded_corpus()
      graded_corpus = load_graded_corpus()

      print("Ungraded Corpus Size : {}".format(len(ungraded_corpus)))
      print("Graded Corpus Size   : {}".format(len(graded_corpus)))

      if args.test_grade_predictor:
         grade_predictor_test(graded_corpus)
      
      if args.test_continuity:
         continuity_test(graded_corpus, ungraded_corpus)
      
   if len(args.essay_filepaths) > 0:
            
      # verify existence of essays
      for essay_filepath in args.essay_filepaths:
         if not os.path.exists(essay_filepath):
            print("Unable to find essay at '{}'".format(essay_filepath))
   
      # load the essays before the grade predictor so if something is wrong
      # with one or more of the essays we can catch it early
      def load_essay(filepath):
         with open(filepath, 'r') as f:
            return f.read().strip()

      essay_contents = []
      for essay_filepath in args.essay_filepaths:
         content = load_essay(essay_filepath)
         if len(content) <= 0:
            print("Essay at '{}' has no content!".format(essay_filepath))
         essay_contents.append(content)

      print("Loading Grade Predictor")
      grade_predictor = load_grade_predictor((not args.pass_fail), args.retrain)

      for essay_filepath, essay_content in zip(args.essay_filepaths, essay_contents):
         if len(essay_content) <= 0: continue

         feedback = get_written_response(essay_content, grade_predictor)

         print("Feedback for essay at '{}'".format(essay_filepath))
         print(feedback)
         print("\n")


def load_grade_predictor(use_a_to_f, overwrite = False):
   
   clf_file = A_F_MODEL if use_a_to_f else PASS_FAIL_MODEL
   
   has_model = os.path.exists(clf_file)
   if not has_model or overwrite:
      if not has_model:
         print("Model not found, training one.")
      elif overwrite:
         print("Retrain option set, retraining model.")

      graded_corpus = load_graded_corpus()
      grade_predictor = Grade_Predictor(use_a_to_f = use_a_to_f, use_known_best = True)
      grade_predictor.train(graded_corpus)
      
      with open(clf_file, 'wb') as f:
         pickle.dump(grade_predictor, f) 

      return grade_predictor
   else:
      print("Loaded Trained Model")
      with open(clf_file, 'rb') as f:
         return pickle.load(f)
   

def get_written_response(essay, grade_predictor):
   """
   Essay should be in plain text format with paragraphs separated by new lines.
   """
   essay_features = grade_predictor.create_feature_set(essay)
   grade = grade_predictor.grade(essay)
   baseline_features = grade_predictor.grade_statistics

   response_dict = written_response.written_response(essay_features, grade, 
                                                      baseline_features)

   text = response_dict['Introduction'] + "\n\n"

   for key, value in response_dict.items():
      if key == 'Introduction':
         continue
      else:
         text += key + ":\n" + value + "\n"

   return text     


def grade_predictor_test(graded_corpus):
   print("=== Running Grade Predictor Test ===")
   k = 10 
   fold_results = []
   fold_scores = []
   fold_accs = []
   preds = []
   for fold_num, grade_train, grade_test in validation.kfold_validation(graded_corpus, k=k):
      print("Fold {:d} / {:d}".format(fold_num+1, k))

      grade_predictor = Grade_Predictor()
      grade_predictor.train(grade_train)
      results = grade_predictor.test(grade_test)
      preds.extend(results)
      
      
      truth, pred = zip(*results)
      
      results_score = precision_recall_fscore_support(truth, pred, average='weighted')
      fold_scores.append(results_score) 

      accuracy = util.prediction_accuracy(results)
      fold_accs.append(accuracy) 
      print("Accuracy  : {:7.2%}".format(accuracy))
      print("Precision : {:.3f}".format(results_score[0]))
      print("Recall    : {:.3f}".format(results_score[1]))
      print("F Score   : {:.3f}".format(results_score[2]))
      print(results_score[3])

      if grade_predictor.bin_type:
         print("Binned accuracy\n{}".format(grade_predictor.binned_accuracy(results)))
   
   
   grouped_scores = tuple(zip(*fold_scores))
   avg_prec = np.mean(grouped_scores[0])
   avg_recall = np.mean(grouped_scores[1])
   avg_fscore = np.mean(grouped_scores[2])

   avg_acc = np.mean(fold_accs)
   print("\n\nAverages over {:d} Folds".format(k))
   print("Accuracy  : {:7.2%}".format(avg_acc))
   print("Precision : {:.3f}".format(avg_prec))
   print("Recall    : {:.3f}".format(avg_recall))
   print("F Score   : {:.3f}".format(avg_fscore))
   print("")
   if grade_predictor.bin_type:
      print("Overall Binned Accuracy over {:d} Folds: {}".format(k, 
                                       grade_predictor.binned_accuracy(preds)))



def continuity_test(graded_corpus, ungraded_corpus):
   print("=== Running Continuity Test ===")
   #grade_predictor = Grade_Predictor()
   #util.print_stat_dict(grade_predictor.get_grade_statistics(graded_corpus))
   #topic_analysis.test_topic_analysis(ungraded_corpus)
   #simp_topic_analysis.test_topic_analysis(ungraded_corpus)
   topic_tests.test_continuity(ungraded_corpus, processes = 4)
   #grade_predictor.train(graded_corpus)
   #print(get_written_response("\n".join(ungraded_corpus[0].paragraphs), grade_predictor))


def parse_args():
   parser = argparse.ArgumentParser(description = "Run grade prediction")
   parser.add_argument("essay_filepaths", metavar = "Essay Filepath", type = str, 
                       nargs = '*',
                       help = "path to plain text essay files where newlines denote new paragraphs.")

   parser.add_argument("-pf", "--pass_fail", action = "store_true", help = 
                       "if set then the essay feedback will be based on Pass/Fail grades rather than A-F")

   parser.add_argument("--retrain", action = "store_true", help = 'if set then the grade predictor classifier will be retrained')

   parser.add_argument("-tgp", "--test_grade_predictor", action = "store_true", help = 
                       "runs a grade predictor test")
   parser.add_argument("-tc", "--test_continuity", action = "store_true", help = 
                       "runs a continuity test")
   
   args = parser.parse_args()
   if len(args.essay_filepaths) <= 0:
      if not args.test_grade_predictor and not args.test_continuity:
         print("Not given an essay to given feedback on nor given a test to run!\n")
         parser.print_help()
         print("")
         sys.exit(1)

   return parser.parse_args()


# Corpus Loading Functions ----------------------------------------------------
def load_ungraded_corpus():
   return ungraded_essay_parser.parse_ungraded_papers(get_ungraded_corpus_file())

def load_graded_corpus():
   return graded_essay_parser.parse_graded_essays(get_graded_corpus_file())


def get_ungraded_corpus_file(filename = UNGRADED_CORPUS_FILE):
   return get_corpus_file(UNGRADED_CORPUS_DIR, filename) 

def get_graded_corpus_file(filename = GRADED_CORPUS_FILE):
   return get_corpus_file(GRADED_CORPUS_DIR, filename)

def get_corpus_file(corpus_name, filename):
   corpus_dir = os.path.join(CORPORA_DIR, corpus_name)

   if not os.path.exists(corpus_dir):
      raise OSError("Corpus named '{}' doesn't exist! Did you decompress it?".format(corpus_name))

   return os.path.join(corpus_dir, 'data', filename)


if __name__ == "__main__":
   main()

