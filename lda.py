
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation
import scipy
import numpy as np
from util import handle_kwargs
import sys
import time

EPSILON = 0.001

def make_token_pattern(min_word_len = 3):
   return r"[a-zA-Z]{" + str(min_word_len) + r",}"

class LDA(object):
   def __init__(self, n_topics, input_vectorizer_type, **kwargs):
      self._make_input_vectorizer(input_vectorizer_type, **kwargs)
      self._make_lda(n_topics, **kwargs)

   def _make_input_vectorizer(self, input_vectorizer_type, **kwargs):
      vectorizer_defaults = {'max_features' : None,
                             'max_df' : 1.0,
                             'min_df' : 0.05,
                             'ngram_range' : (1,1),
                             'token_pattern' : make_token_pattern(3),
                             'stop_words' : 'english',
                             'lowercase' : True}
      
      self.vectorizer_kwargs = handle_kwargs(vectorizer_defaults, kwargs)

      # create our input vectorizer
      self.input_vectorizer = input_vectorizer_type(**self.vectorizer_kwargs)
  

   def _make_lda(self, n_topics, **kwargs):
      lda_defaults = {'n_topics': n_topics,
                      'max_iter' : 10,
                      'learning_method' : 'online',
                      'learning_offset' : 10.0,
                      'learning_decay' : 0.7,
                      'evaluate_every' : -1,
                      'batch_size' : 128,
                      'verbose' : 0,
                      'n_jobs' : 4,
                      'random_state' : 0}

      self.lda_kwargs = handle_kwargs(lda_defaults, kwargs)
      self.lda = LatentDirichletAllocation(**self.lda_kwargs)
      self.n_topics = n_topics
   
   '''Gets words included in input vectorizer'''
   def get_feature_names(self):
      return self.input_vectorizer.get_feature_names()

   # Fitting LDA to Data ------------------------------------------------------
   def train(self, training_set, verbose = False):
      """
      Trains the LDA model on the given training set.

      Arguments:
         |training_set| a list of strings representing the training set
      """
      if verbose:
         print("Training LDA Model on {} items".format(len(training_set)))
         sys.stdout.write("Vectorizing Input...\r")
         sys.stdout.flush()

      vec_start = time.time()
      vec_input = self.input_vectorizer.fit_transform(training_set)

      if verbose:
         print("Vectorized Input in {:.3f} seconds".format(time.time() - vec_start))
         sys.stdout.write("Fitting LDA Model...\r")
         sys.stdout.flush()

      lda_start = time.time()
      self.lda.fit(vec_input)

      if verbose:
         print("Fit LDA Model with {} topics in {:.3f} seconds".format(self.n_topics, time.time() - lda_start))
 
   def fit_essay(self, essay):
      self.train(essay.get_sentences())

   # Making Topic Predictions -------------------------------------------------
   def predict(self, text_list, generalize = False):
      """
      Makes a topic prediction on the given list of text

      Arguments:
         |text_list| a list of text to makes predictions on.

      Returns a 2D array of predictions. Each row corresponds to one item in the
         given |text_list|.
      """
      vec_input = self.input_vectorizer.transform(text_list)
      return self.lda.transform(vec_input)

   def predict_generalized(self, text_list):
      top_preds = [self.get_top_preds(preds) for preds in self.predict(text_list)]

      all_preds = []
      comps = self.lda.components_
      for top_pred in top_preds:
         generalized_topic = np.zeros(len(comps[0])) 
         for topic_id, conf in top_pred:
            generalized_topic = np.add(generalized_topic, comps[topic_id] * conf)
         all_preds.append(generalized_topic)
      return all_preds

   def predict_best_only(self, text_list):
      return [self.get_top_preds(preds)[0] for preds in self.predict(text_list)]

   # Printing/Gettings Top Words ---------------------------------------------- 
   def get_topic_words_str(self, topic_id, n_top_words = None):
      feature_names = self.get_feature_names()
      topic = self.lda.components_[topic_id]
      if n_top_words is None:
         # getting n top words
         return (''.join([feature_names[i] + ' ' + str(round(topic[i], 3))
               +' | ' for i in range(len(topic))]))
      else:
         # getting all words
         return (''.join([feature_names[i] + ' ' + str(round(topic[i], 3))
               +' | ' for i in topic.argsort()[:-n_top_words - 1:-1]]))


   def print_top_words(self, n_top_words = None):
      """
      Prints out the top words for each topic learned by the LDA model.
      
      Optional:
         |n_top_words| defaults to None, the number of words to print out for 
            each topic. If None then all words will be printed
      """
      feature_names = self.get_feature_names()
      for topic_id, topic in enumerate(self.lda.components_):
         print('\nTopic #%d:' % int(topic_id + 1)) 
         print(self.get_topic_words_str(topic, n_top_words))

   
   '''given predictions from predict(), gets a sorted list of the best ones'''
   def get_top_preds(self, preds):
      topic_preds = enumerate(preds)
      top_preds = [(topic, pred) for topic, pred in topic_preds if pred > (1.0/self.n_topics)]
      return sorted(top_preds, key = lambda x : x[1], reverse = True)
   #def print_top_preds(self, preds, num_preds = None):

   
   '''gets a list of words that make up the given topic under the given conditions'''
   def get_topic_words(self, topic, n_words = None, min_word_bound = None):
      """
      Gets the words associated with the given topic

      Arguments:
         |word_dist| the word distribution that makes up the given topic or,
            if an integer a topic number

      Optional:   
         |n_words| the number of words to get for the topic

      Returns a list of the words associated with the given topic
      """
      feature_names = self.get_feature_names()
      
      if type(topic) == int:
         topic = self.lda.components_[topic_num]
      
      word_vals = []
      for i, val in enumerate(topic):
         word_vals.append((feature_names[i], val))
      
      word_vals = sorted(word_vals, key = lambda x : x[1], reverse = True)
      
      if min_word_bound is None:
         min_word_bound = 1.0 / self.n_topics
      
      bounded_words = [w for w,v in word_vals if v > min_word_bound]

      if n_words is None:
         return bounded_words
      else:
         return bounded_words[:n_words]
 

   '''useful info about how many topics were fit well'''
   def _get_distribution_info(self):
      vals = []
      for topic_id, topic in enumerate(self.lda.components_):
         vals.extend(topic)
      return {'avg' : np.mean(vals), 
              'std' : np.std(vals),
              'min' : min(vals),
              'max' : max(vals)}

   def topic_usage(self):
      dist_info = self._get_distribution_info()
      avg_val = dist_info['avg']
      min_val = dist_info['min']

      useless_topics = []
      useful_topics = []
      for topic_id, topic in enumerate(self.lda.components_):
         if np.mean(topic) <= (1.0 / self.n_topics) + EPSILON:
            useless_topics.append(topic_id+1)
         else:
            useful_topics.append(topic_id+1)
      
      usage_p = len(useful_topics) / (len(useful_topics) + len(useless_topics))
      return useless_topics, useful_topics, usage_p

   def print_topic_usage(self, name = "Topic Usage", topic_usage = None):
      if topic_usage is None:
         topic_usage = self.topic_usage()
      
      useless, useful, usage = topic_usage
      print(name)
      print("  Useless Topics [{}] : {}".format(len(useless), useless))
      print("  Useful Topics  [{}] : {}".format(len(useful), useful))
      print("  Usage : {:7.2%}".format(usage))
      print("")




